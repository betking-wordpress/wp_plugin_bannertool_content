<?php
/**
 * Created by PhpStorm.
 * User: Karol
 * Date: 16/4/2021
 * Time: 09:47
 */

namespace PluginBannertoolContent;

use Timber\Timber;

class BannertoolContent
{
    public function renderContent($zoneId,$templateName){
        //TIMEOUT
        $context = stream_context_create(array('http' => array('timeout' => 10)));
        //GET DATA
        $contents = file_get_contents("https://bannerservice.jackpotkings.ag/api/v1/zones/$zoneId?token=YamnYaR6Akwahlp4cYmEB2eLyNnXNmIt0fpEJf7qTIw=1", 0, $context);
        if (!empty($contents)) {
            $data = json_decode($contents);
            $context = array();
            $context["data"] = $data->zone->banners;
            return Timber::render("resources/html/$templateName.twig", $context);
        }else{
            return "";
        }
    }

    public static function loadJs(){
        wp_register_script('bannertoolContent', "/wp-content/plugins/wp_plugin_bannertool_content/resources/js/bannertool_content.js", false, '1.0.0', true);
        wp_enqueue_script('bannertoolContent');
    }

}