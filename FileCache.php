<?php
namespace PluginBannertoolContent;

trait FileCache
{

    private static function fileIsRecent($time, $fileName)
    {

        $age = time() - filemtime($fileName);

        return $time > $age;

    }

    public static function isFileCacheValid($time, $fileName)
    {
        if (!file_exists($fileName) || isset($_GET['cache'])) {
            return false;
        }

        return self::fileIsRecent($time, $fileName);
    }

    public static function cacheFile($fileName, $data, $is_minified)
    {
        if($is_minified){
            file_put_contents($fileName, self::minifyHtml($data));
        }
        else{
            file_put_contents($fileName, $data);
        }

    }

    public static function getCacheFile($fileName)
    {
        return file_get_contents($fileName, true);
    }

    private static function minifyHtml($html)
    {
        return preg_replace('#(?ix)(?>[^\S ]\s*|\s{2,})(?=(?:(?:[^<]++|<(?!/?(?:textarea|pre)\b))*+)(?:<(?>textarea|pre)\b|\z))#', ' ', $html);
    }
}