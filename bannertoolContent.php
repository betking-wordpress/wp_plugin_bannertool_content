<?php
/*
 *
Plugin Name: BannerTool Content
Plugin URI: https://www.jazzsports.ag
Description: this plugin creates content from the information provided by banner tool
Version: 0.1
Author: Jarce
Author URI: https://www.jazzsports.ag
License: jazzsports
Text Domain: BannerTool Content
*/

require_once("class.BannertoolContent.php");
require_once("FileCache.php");

use PluginBannertoolContent\BannertoolContent;
use PluginBannertoolContent\FileCache;

//SHORTCODE BANNERTOOL CONTENT
function bannerToolContentShortcode($attr)
{
    $base64 = base64_encode(json_encode($attr));
    $cachePathCasino = __DIR__ . "/cache/bannertool_content_$base64.html";
    if (FileCache::isFileCacheValid(300, $cachePathCasino)) {
        print_r(FileCache::getCacheFile($cachePathCasino));
    } else {
        if (isset($attr["zone"]) && isset($attr["template_name"])) {
            $oBannertool = new BannertoolContent();
            $result = $oBannertool->renderContent($attr["zone"], $attr["template_name"]);
            //THIS FUNCTION CREATE THE CACHE
            FileCache::cacheFile($cachePathCasino, $result, true);
        }
    }
    add_action('wp_footer', function () {
        BannertoolContent::loadJs();
    });
}

add_shortcode('bannertool-content', 'bannerToolContentShortcode');
